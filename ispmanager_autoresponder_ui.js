(function () {
    var R = React;
    var D = React.DOM;
    var cmd = function () {
        return window.ispmanager_autoresponder.cmd;
    };
    var utils = window.ispmanager_autoresponder.utils;
    var ui = window.ispmanager_autoresponder.components;
    var getLabel = utils.getLabel;
    var capitalize = utils.capitalize;
    var ImmutableRenderMixin = utils.ImmutableRenderMixin;

    /*
     * Components
     */

    ui.FromField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({for: "autoresponder-from"}, capitalize(getLabel('from'))),
                    D.input({
                        id: 'autoresponder-from',
                        type: 'text',
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateFrom(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.SubjectField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({for: "autoresponder-subject"}, capitalize(getLabel('subject'))),
                    D.input({
                        id: 'autoresponder-subject',
                        type: 'text',
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateSubject(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.BodyField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({for: "autoresponder-body"}, capitalize(getLabel('body'))),
                    D.textarea({
                        id: 'autoresponder-body',
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            cmd().updateBody(e.target.value);
                        }
                    })
                ])
            );
        }
    });

    ui.ActiveField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.span(null, [
                    D.label({for: "autoresponder-active"}, capitalize(getLabel('active'))),
                    D.input({
                        id: 'autoresponder-active',
                        type: 'checkbox',
                        checked: this.props.data,
                        onChange: function (e) {
                            cmd().updateActive(e.target.checked);
                        }
                    })
                ])
            );
        }
    });

    ui.AutoresponderForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div(null, [
                    D.div({className: 'autoresponder-field'},
                        R.createElement(ui.FromField, {data: this.props.data.get('from')})
                    ),
                    D.div({className: 'autoresponder-field'},
                        R.createElement(ui.SubjectField, {data: this.props.data.get('subject')})
                    ),
                    D.div({className: 'autoresponder-field'},
                        R.createElement(ui.BodyField, {data: this.props.data.get('body')})
                    ),
                    D.div({className: 'autoresponder-field'},
                        R.createElement(ui.ActiveField, {data: this.props.data.get('active')})
                    ),
                    D.div({className: "buttons"},
                        D.input({
                            type: 'button',
                            className: 'mainaction button',
                            value: capitalize(getLabel('savechanges')),
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().persistAutoresponder();
                            }
                        })
                    )
                ])
            );
        }
    });

    ui.AutoresponderView = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({
                        id: "filter-box",
                        className: "uibox contentbox",
                        style: {left: 0, padding: "10px", overflowY: "scroll"}
                    },
                    R.createElement(ui.AutoresponderForm, this.props)
                )
            );
        }
    });
})();

