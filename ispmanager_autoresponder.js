"use strict";

/*
 * For localization and templating purposes. Proper JS files
 * are included separately.
 */
window.ispmanager_autoresponder = {
    labels: {},
    components: {},
    cmd: {}
};

/*
 * Common utilities used across the componenets and UI
 */
window.ispmanager_autoresponder.utils = {
    capitalize: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    getLabel: function (label, params) {
        params = params || {};
        var labels = window.ispmanager_autoresponder.labels;
        var output = label;

        if (labels.hasOwnProperty(label)) {
            output = labels[label];
        }

        for (var param in params) {
            if (params.hasOwnProperty(param)) {
                output = output.replace('{' + param + '}', params[param]);
            }
        }

        return output;
    },
    // Code taken from https://github.com/jurassix/react-immutable-render-mixin
    ImmutableRenderMixin: (function () {
        function shallowEqualImmutable(objA, objB) {
            if (objA === objB || Immutable.is(objA, objB)) {
                return true;
            }

            if (typeof objA !== 'object' || objA === null ||
                typeof objB !== 'object' || objB === null) {
                return false;
            }

            var keysA = Object.keys(objA);
            var keysB = Object.keys(objB);

            if (keysA.length !== keysB.length) {
                return false;
            }

            // Test for A's keys different from B.
            var bHasOwnProperty = Object.prototype.hasOwnProperty.bind(objB);
            for (var i = 0; i < keysA.length; i++) {
                if (!bHasOwnProperty(keysA[i])
                    || !Immutable.is(objA[keysA[i]], objB[keysA[i]])) {
                    return false;
                }
            }

            return true;
        }

        return {
            shouldComponentUpdate: function (nextProps, nextState) {
                return !shallowEqualImmutable(this.props, nextProps)
                    || !shallowEqualImmutable(this.state, nextState);
            }
        }
    })()
};

/*


 */
window.IspmanagerAutoresponderUI = function (rcube_webmail, rcmail, containerId) {
    /*
     * App state
     */
    var State = Immutable.fromJS({
        from: "",
        subject: "",
        content: "",
        active: false
    });

    /*
     * Helpers
     */

    var utils = window.ispmanager_autoresponder.utils;

    function capitalize(string) {
        return utils.capitalize(string);
    }

    function getLabel(label, params) {
        return utils.getLabel(label, params);
    }

    function objPosition(obj) {
        var curleft = 0, curtop = 0;

        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
        }

        return [curleft, curtop];
    }

    function showConfirmPopup(content, title, actionLabel, action) {
        var buttons = {};

        buttons[actionLabel] = function (e) {
            action();
            popup.dialog('close');
        };

        buttons[capitalize(getLabel('cancel'))] = function (e) {
            popup.dialog('close');
        };

        var popup = rcmail.show_popup_dialog(
            content,
            title,
            buttons,
            {
                button_classes: ['mainaction'],
                modal: true,
                closeOnEscape: true,
                close: function (e, ui) {
                    rcmail.ksearch_hide();
                    $(this).remove();
                }
            }
        );
    }

    function asList(n) {
        if (n && !Immutable.List.isList(n)) {
            return Immutable.List.of(n);
        }

        if (n) {
            return n;
        }

        return Immutable.List();
    }

    /*
     * Entities
     */

    var Model = {};

    Model.Autoresponder = function (input) {
        return Immutable.fromJS({
            from: input.getIn(['arfrom', '$']),
            subject: input.getIn(['arsubj', '$']),
            body: input.getIn(['arbody', '$']),
            active: input.getIn(['arenable', '$']) == 'on'
        });
    };

    /*
     * Response dispatch and handling
     */

    var Handler = {};

    Handler.updateAutoresponder = function (response) {
        State = State.update(function (autoresponder) {
            return autoresponder.merge(Model.Autoresponder(response));
        });
        render();
    };

    Handler.updateAutoresponderSuccess = function (response) {
        Cmd.fetchAutoresponder();
        rcmail.display_message(getLabel('savedchanges'), "confirmation");
    };

    Handler.defaultError = function (response) {
        rcmail.display_message(response.getIn(['error', 'msg', '$']), "warning");
    };

    // handlers mapping
    var handlers = {
        'email.responder': Handler.updateAutoresponder
    };

    var successHandlers = {
        'email.responder': Handler.updateAutoresponderSuccess
    };

    var errorHandlers = {
        default: Handler.defaultError
    };

    // handlers dispatch entry point
    rcube_webmail.prototype.ispmanager_autoresponder_handle_response = function (_, o) {
        console.log(o);

        var isOk = o.doc.ok;
        var isError = o.doc.error;

        if (isOk) {
            successHandlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        } else if (isError) {
            // FIXME: better error handling TBD
            var action = 'default';
            errorHandlers[action](Immutable.fromJS(o.doc));
        } else {
            handlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        }
    };

    /*
     * Commands
     */

    // Low-level API call
    function apiCall(action, params) {
        params['func'] = action;

        rcmail.http_post(
            'plugin.ispmanager_autoresponder-action',
            '_params=' + encodeURIComponent(JSON.stringify(params))
        );
    }

    var Cmd = {};

    Cmd.fetchAutoresponder = function () {
        apiCall('email.responder', {elid: '_USER'});
    };

    Cmd.persistAutoresponder = function () {
        apiCall('email.responder', {
            sok: 'ok',
            elid: '_USER',
            arfrom: State.get('from'),
            arsubj: State.get('subject'),
            arbody: State.get('body'),
            arenable: State.get('active')?'on':'off'
        });
    };

    Cmd.updateFrom = function (from) {
        State = State.set('from', from);
        render();
    };

    Cmd.updateSubject = function (subject) {
        State = State.set('subject', subject);
        render();
    };

    Cmd.updateBody = function (body) {
        State = State.set('body', body);
        render();
    };

    Cmd.updateActive = function (active) {
        State = State.set('active', active);
        render();
    };

    // Cmd is added to a public object
    // to be reachable for components
    window.ispmanager_autoresponder.cmd = Cmd;

    /*
     * Rendering function
     */
    function render() {
        var ui = window.ispmanager_autoresponder.components;

        React.render(
            React.createElement(ui.AutoresponderView, {data: State}),
            document.getElementById(containerId)
        );
    }

    /*
     * Public API
     */
    return {
        init: function () {
            Cmd.fetchAutoresponder();
        }
    }
};

