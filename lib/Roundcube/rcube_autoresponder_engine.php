<?php

class rcube_autoresponder_engine
{
    private $rc;
    private $plugin;
    private $api;

    /**
     * Class constructor
     */
    function __construct($plugin)
    {
        $this->rc = rcube::get_instance();
        $this->plugin = $plugin;
    }

    private function getAPI()
    {
        if (!$this->api) {
            $username = $_SESSION['username'];
            $password = $this->rc->decrypt($_SESSION['password']);
            $apiUrl = $this->rc->config->get('ispmanager_autoresponder_api_url', 'https://localhost:1500/ispmgr');
            $debug = $this->rc->config->get('ispmanager_autoresponder_debug', false);

            $this->api = new rcube_autoresponder_api($username, $password, $apiUrl, $debug);
        }

        return $this->api;
    }

    function actions()
    {
        $params = rcube_utils::get_input_value('_params', rcube_utils::INPUT_GPC);
        if ($params) {
            $paramsArray = json_decode(urldecode($params), true);
            $response = $this->getAPI()->query($paramsArray);

            $this->rc->output->command(
                'ispmanager_autoresponder_handle_response', 'response', $response);
        } else {
            $this->send();
        }
    }

    function send()
    {
        $this->rc->output->set_pagetitle($this->plugin->gettext('autoresponders'));
        $this->rc->output->send('ispmanager_autoresponder.manage');
    }
}
